package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Metallica {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Metallica() {
    }
    
    public ArrayList<Song> getMetallicaSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("One", "Metallica");             				//Create a song
         Song track2 = new Song("Unforgiven", "Metallica");         			//Create another song
         Song track3 = new Song("Enter Sandman", "Metallica");         			//Create another song
         Song track4 = new Song("The Day that Never Comes", "Metallica");       //Create another song
         Song track5 = new Song("Frantic", "Metallica");         				//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Metallica
         this.albumTracks.add(track2); 											//Add the second song to song list for Metallica
         this.albumTracks.add(track3); 											//Add the third song to song list for Metallica
         this.albumTracks.add(track4);
         this.albumTracks.add(track5);
         return albumTracks;                                                    //Return the songs for Metallica in the form of an ArrayList
    }
}
