package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class RollingStones {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public RollingStones() {
    }
    
    public ArrayList<Song> getRollingStonesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Paint it Black", "The Rolling Stones");         				//Create a song
         Song track2 = new Song("Satisfaction", "The Rolling Stones");        //Create another song
         this.albumTracks.add(track1);                                  //Add the first song to song list
         this.albumTracks.add(track2);                                  //Add the second song to song list
         return albumTracks;                                            //Return the songs for The Rolling Stones in the form of an ArrayList
    }
}