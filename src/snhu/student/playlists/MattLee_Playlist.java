package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class MattLee_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> GreenDayTracks = new ArrayList<Song>();
    GreenDay GreenDayBand = new GreenDay();
	
    GreenDayTracks = GreenDayBand.getGreenDaySongs();
	
	playlist.add(GreenDayTracks.get(0));
	playlist.add(GreenDayTracks.get(1));
	
	
    RollingStones RollingStonesBand = new RollingStones();
	ArrayList<Song> RollingStonesTracks = new ArrayList<Song>();
    RollingStonesTracks = RollingStonesBand.getRollingStonesSongs();
	
	playlist.add(RollingStonesTracks.get(0));
	playlist.add(RollingStonesTracks.get(1));
	
	Rancid RancidBand = new Rancid();
	ArrayList<Song> RancidTracks = new ArrayList<Song>();
    RancidTracks = RancidBand.getRancidSongs();
    
    playlist.add(RancidTracks.get(0));
	
    return playlist;
	}
}
