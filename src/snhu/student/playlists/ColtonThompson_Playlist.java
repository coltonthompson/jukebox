package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;

import music.artist.Metallica;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;

public class ColtonThompson_Playlist {

	/**
	 * Pulls Colton Thompson's playlist.
	 * 
	 * @return	
	 * 	the playlist object which contains a list of songs to be played.
	 */
	public LinkedList<PlayableSong> StudentPlaylist() {
		// Create the list of all the songs so we can add to it later.
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
		// Create the band object from the band class we added before.
	    Metallica metallicaBand = new Metallica();
	    // Create the list of tracks from the band that we wish to add.
		ArrayList<Song> metallicaTracks = new ArrayList<Song>();
		
		// Pull the tracks from the band and pull which songs we want on our playlist.
	    metallicaTracks = metallicaBand.getMetallicaSongs();
		playlist.add(metallicaTracks.get(0));
		playlist.add(metallicaTracks.get(1));
		playlist.add(metallicaTracks.get(2));
		playlist.add(metallicaTracks.get(3));
		playlist.add(metallicaTracks.get(4));
		
		// Return the completed playlist.
	    return playlist;
	}
	
}
